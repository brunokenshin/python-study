import tkinter as tk
from tkinter import filedialog
from simple_colors import *

def find_words():
    print("*********************************")
    print("**********Caça Palavras**********")
    print("*********************************\n")

    # 1. Pedir para inserir o arquivo de letras (Ler a matriz de letras):
    root = tk.Tk()
    root.withdraw()
    file_path = filedialog.askopenfilename()

    matrix_file = open(file_path)

    print("Matriz de letras importada: \n")

    words_matrix = buildmatrix(matrix_file)

    printmatrix(words_matrix)

    # 2. Pedir palavra para ser procurada
    word_to_find = input("\nQual palavra você quer buscar? ").upper()

    print("Buscando palavra: " + word_to_find + "\n")

    # 3. Localizar a palavra na matriz e armazenar as coordenadas
    solution_coordinates = findsolutioncoordinates(words_matrix, word_to_find)

    # 4. Exibir a matriz com a palavra marcada
    printmatrix(words_matrix, solution_coordinates)

def buildmatrix(file):
    matrix = []
    
    for line in file.readlines():
        row = line.split(" ")
        matrix.append(row)

    return matrix

def printmatrix(matrix, coordinates = []):
    print(" ", end="")

    for i in range(len(matrix)):
        for j in range(len(matrix[i])):
            if(isSelectedCoordinate(i, j, coordinates)):
                # Colorido
                print(red(matrix[i][j], ['bold']), end=" ")  
            else:
                # Branco
                print(matrix[i][j], end=" ")
    print()

def isSelectedCoordinate(row, column, coordinates):
    for coordinatePair in coordinates:
        if(coordinatePair[0] == row and coordinatePair[1] == column):
            return True 
            
    return False

# TODO: Terminar de implementar
def findsolutioncoordinates(words_matrix, word_to_find):
    letters_from_word = list(word_to_find)

    solution_coordinates = []

    for letter_index, letter in enumerate(letters_from_word):
        for row in range(len(words_matrix)):
            for column in range(len(words_matrix[row])):
                if(letter == words_matrix[row][column]):
                    current_coordinates = (row, column)
                    # searchforneighbors(words_matrix, current_coordinates, letters_from_word, letter_index)
            
    # Ex: GREGORIO
    return [(0,1), (1,2), (2,3), (3,4), (4,5), (5,6), (6,7), (7,8)]

# TODO: essa função deve ser recursiva
def searchforneighbors(words_matrix, current_coordinates, letters_from_word, letter_index):
    return

def getnorthneighborindex(current_coordinates, matrix):
    north_coordinates = (current_coordinates[0] - 1, current_coordinates[1])
    return getneighborByCoordinates(matrix, north_coordinates)
    
def getnortheastneighborindex(current_coordinates, matrix):
    northeast_coordinates = (current_coordinates[0] - 1, current_coordinates[1] + 1)
    return getneighborByCoordinates(matrix, northeast_coordinates)

def geteastneighborindex(current_coordinates, matrix):
    east_coordinates = (current_coordinates[0], current_coordinates[1] + 1)
    return getneighborByCoordinates(matrix, east_coordinates)
    
def getsoutheastneighborindex(current_coordinates, matrix):
    southeast_coordinates = (current_coordinates[0] + 1, current_coordinates[1] + 1)
    return getneighborByCoordinates(matrix, southeast_coordinates)

def getsouthneighborindex(current_coordinates, matrix):
    south_coordinates = (current_coordinates[0] + 1, current_coordinates[1])
    return getneighborByCoordinates(matrix, south_coordinates)

def getsouthwestneighborindex(current_coordinates, matrix):
    southwest_coordinates = (current_coordinates[0] + 1, current_coordinates[1] - 1)
    return getneighborByCoordinates(matrix, southwest_coordinates)

def getwestneighborindex(current_coordinates, matrix):
    west_coordinates = (current_coordinates[0], current_coordinates[1] - 1)
    return getneighborByCoordinates(matrix, west_coordinates)

def getnorthwestneighborindex(current_coordinates, matrix):
    northwest_coordinates = (current_coordinates[0] - 1, current_coordinates[1] - 1)
    return getneighborByCoordinates(matrix, northwest_coordinates)

def getneighborByCoordinates(matrix, neighbor_coordinates):
    try:
        existent_neighbor = getelementbycoordinates(matrix, neighbor_coordinates)
        return (neighbor_coordinates, existent_neighbor)
    except IndexError:
        return None

def getelementbycoordinates(matrix, coordinates):
    return matrix[coordinates[0]][coordinates[1]]

if (__name__ == '__main__'):
    find_words()