import random


def jogar():
    print("*********************************")
    print("Bem vindo ao jogo de Adivinhação!")
    print("*********************************")

    total_de_tentativas = 0
    chute_minimo = 1
    chute_maximo = 100
    total_de_pontos = 500

    print("(1) Fácil (2) Médio (3) Difícil")
    nivel = int(input("Defina o nível: "))

    if (1 == nivel):
        total_de_tentativas = 15
    elif (3 == nivel):
        total_de_tentativas = 5
    else:
        total_de_tentativas = 10

    numero_secreto = random.randrange(chute_minimo, chute_maximo + 1)

    for rodada in range(1, total_de_tentativas + 1):
        print("Tentativa {} de {}".format(rodada, total_de_tentativas))
        chute_str = input("Digite um número entre {} e {}: ".format(chute_minimo, chute_maximo))
        print("Você digitou: ", chute_str)
        chute = int(chute_str)

        if (chute < 1 or chute > 100):
            print("Você deve digitar um númer entre {} e {}".format(chute_minimo, chute_maximo))
            continue

        acertou = chute == numero_secreto
        maior   = chute > numero_secreto
        menor   = chute < numero_secreto
        pontos_perdidos = numero_secreto - chute

        if (acertou):
            print("Você acertou e fez {} pontos!".format(total_de_pontos))
            break
        elif (maior):
            print("Você chutou um número maior que o número secreto")
        elif (menor):
            print("Você chutou um número menor que o número secreto")

        total_de_pontos = abs(total_de_pontos - pontos_perdidos)

    print("Fim do jogo")


if(__name__ == "__main__"):
    jogar()